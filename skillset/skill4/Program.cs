﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string requirements =
            @"Program requirements:
            1) use intrinsic method to display date/time
            2) use four types of iteration structures
            3) initialize suitable variables
            4) loop through number 1-10
            5) create string array for the days of the week
            6)use foreach to loop through the days of the week
            7)allow users to press any key to exit";
            
            Console.Write(requirements);
            DateTime DT = DateTime.Now;
            Console.WriteLine("Now:"+ DT.ToString());
            Console.WriteLine("for loop: ");
            int n = 1;
            for (int i = 1; i<=10; i++){
                Console.Write(i+", ");
                
            }
            Console.WriteLine("\nWhile loop:");
            while (n<= 10){
                Console.Write(n+", ");
                n++;
            }
            Console.WriteLine("\nDo..While loop:");
            n = 1;
            do{
                Console.Write(n+", ");
                n++;
            }
            while(n<=10);
            Console.WriteLine("\nFor each loop");
            string[] days = new string[] {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
            foreach( string day in days){
                Console.Write(day+", ");
            }
            Console.WriteLine("\n press any key to exit");
            Console.ReadKey();
        }
    }
}
