using System;

namespace ConsoleApplication
{
    public class Vehicle
    {
        private float milesTraveled, gallonsUsed;
        public string Manufacturer { get; set;}
        public string Make { get; set;}
        public string Model { get; set;}
        public float MPG {
            get {
                if (gallonsUsed <= 0.0f) {
                    return 1.0f;
                }
                else {
                    return milesTraveled / gallonsUsed;
                }
            }
        }
        public Vehicle (){
            this.Manufacturer = "GM";
            this.Make = "Chevorlet";
            this.Model = "Camaro";
            
            Console.WriteLine("Creating base object from default construstor");
        }
        
        public Vehicle (string Manufacturer = "Manufacturer", string Make = "Make", string Model = "Model"){
            this.Manufacturer = Manufacturer;
            this.Make = Make;
            this.Model = Model;
            
            Console.WriteLine("Creating base object from parameterized constructor");
            
        }
        public void setMiles(float miles = 0.0f){
            milesTraveled = miles;
        }
        public void setGallons(float gallons = 0.0f){
            gallonsUsed = gallons;
        }
        public float getMiles(){
            return milesTraveled;
        }
        public float getGallons(){
            return gallonsUsed;
        }
        public virtual string getObjectInfo(){
            return Manufacturer + ", " + Make + ", " + Model + ", " + MPG;
        }
        public virtual string getObjectInfo(string sep){
            return Manufacturer + sep + Make + sep + Model + sep + MPG;
        }

    }
}
