﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string p_manufacturer, p_make, p_model, p_seperator;
            float p_miles, p_gallons;
            
            Console.Write("Manufacturer (alpha):");
            p_manufacturer = Console.ReadLine();
            
            Console.Write("Make (alpha):");
            p_make = Console.ReadLine();
            
            Console.Write("Modle (alpha):");
            p_model = Console.ReadLine();
            
            Console.Write("Miles: ");
            while(!float.TryParse(Console.ReadLine(), out p_miles)){
                Console.Write("Miles must be numeric");
            }
            Console.Write("Gallons used: ");
            while(!float.TryParse(Console.ReadLine(), out p_gallons)){
                Console.Write("gallons must be numeric");
            }
            Vehicle v1 = new Vehicle(p_manufacturer);
            
            
            Console.Write("Seperator(, : ;): ");
            p_seperator = Console.ReadLine();
            Console.WriteLine(v1.getObjectInfo(p_seperator));
            
            Vehicle v2 = new Vehicle (p_manufacturer, p_make, p_model);
            v2.setMiles(p_miles);
            v2.setGallons(p_gallons);
            
            Console.Write("Seperator(, : ;): ");
            p_seperator = Console.ReadLine();
            
            Console.WriteLine(v2.getObjectInfo(p_seperator));
            
            Console.WriteLine("Demonstrating polymorphism (derived object)");
            
            string p_style;
            
            Console.Write("Manufacturer (alpha):");
            p_manufacturer = Console.ReadLine();
            
            Console.Write("Make (alpha):");
            p_make = Console.ReadLine();
            
            Console.Write("Modle (alpha):");
            p_model = Console.ReadLine();
            
            Console.Write("Miles: ");
            while(!float.TryParse(Console.ReadLine(), out p_miles)){
                Console.Write("Miles must be numeric");
            }
            Console.Write("Gallons used: ");
            while(!float.TryParse(Console.ReadLine(), out p_gallons)){
                Console.Write("gallons must be numeric");
            }
            Console.Write("Style (Alphanumeric): ");
            p_style = Console.ReadLine();
            
            Car c1 = new Car (p_manufacturer, p_make, p_model,p_style);
            c1.setMiles(p_miles);
            c1.setGallons(p_gallons);
            
            Console.Write("Seperator(, : ;): ");
            p_seperator = Console.ReadLine();
            
            Console.WriteLine(c1.getObjectInfo(p_seperator));
            
            
        }
    }
}
