using System;

namespace ConsoleApplication
{
    public class Car : Vehicle
    {
        private string style;
        public string Style{
            get {return style;}
            set {style = value;}
        }
        public Car(){
            this.Style = "Default Style";
            Console.WriteLine("Creating derived object from default constructor (accepts no arguments)");
        }
        public Car (string Manufacturer, string Make, string Model, string Style) : base(Manufacturer, Make, Model) {
            this.Style = Style;
            Console.WriteLine("Creating derived object from parameterized constructor");
        }
        public override string getObjectInfo(string sep){
            return base.getObjectInfo(sep)+sep+Style;
        }

    }
}
