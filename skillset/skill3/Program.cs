﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string requirements =
            @"Program requirements:
            1) Initialize variables
            2) prompt and capture user input
            3) convert into Farenheit and Celsius
            3) allow user to press any key to return to command line";
            
            Console.Write(requirements);
            double bill = 100.00;
            Console.WriteLine("Your bill is: $"+bill);
            string discounts =
            @"Discounts:
            6+: 30% off
            3-5: 20% off
            2: 10% off
            1 regular price";
            Console.Write(discounts);
            Console.Write("\nHow many books did you purchase? ");
            int booksPur = int.Parse(Console.ReadLine());
            if(booksPur <= 1){
                double cost = bill;
                Console.WriteLine("Your cost is: $"+cost);
            }
            else if(booksPur <= 2){
                double cost = bill*0.9;
                Console.WriteLine("Your cost is: $"+cost);
            }
            else if(booksPur < 6){
                double cost = bill*0.8;
                Console.WriteLine("Your cost is: $"+cost);
            }
            else{
                double cost = bill*0.7;
                Console.WriteLine("Your cost is: $"+cost);
            }
            Console.WriteLine("**************SWitch Case**************");
            string color =
            @"
            Red --1
            blue --2
            green --3";
            Console.WriteLine(color);
            Console.Write("What is your favorite color? ");
            int choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
            case 1:
                Console.WriteLine("Your favorite color is red!");
                break;
            case 2:
                Console.WriteLine("Youre favorite color is blue!");
                break;
            case 3:
                Console.WriteLine("Your favorite color is green!");
                break;
            }
            
            Console.Write("press any key to exit");
            Console.ReadKey();
            
            
        }
    }
}
