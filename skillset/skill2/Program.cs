﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string requirements =
            @"Program requirements:
            1) Initialize variables
            2) prompt and capture user input
            3) convert into Farenheit and Celsius
            3) allow user to press any key to return to command line";
            
            Console.WriteLine(requirements);
            
            Console.WriteLine("Is the temperature in Farenheit or Celsius? (f or c respectively)?");
            
           string tempType = Console.ReadLine();
           
           Console.Write(" Please enter the temperature:");
           
           double temp;
           bool tempResult = double.TryParse(Console.ReadLine(), out temp);
           
           if (tempType == "f"){
            double x = farToCel(temp);
            Console.WriteLine("Your temperature in Celsius is: "+ x);
            
           }
           else if ( tempType == "c"){
            double x = celToFar(temp);
            Console.WriteLine("Your temperature in Farenheit is: "+ x);
           }
          Console.Write("Press any key to exit the program");
          Console.ReadKey();
        }
        static double celToFar(double c){
            double f = ((9.0/5.0)*c)+32;
            return f;
            
        }
        static double farToCel(double f){
            double c = ((5.0/9.0)*(f-32));
            return c;
        }
    }
}
