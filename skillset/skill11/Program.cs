﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double num1;
            Console.Write("Number 1: ");
            while(!double.TryParse(Console.ReadLine(), out num1)){
                Console.WriteLine("number 1 must be numeric:");
            }
            Console.WriteLine("call passByVal()");
            passByVal(num1);
            Console.WriteLine("In main method number 1 = "+num1);
            
            Console.WriteLine("\ncall passByRef()");
            passByRef(ref num1);
            Console.WriteLine("In main method number 1 = "+ num1);
            
        }
        public static void passByVal (double num1){
            Console.WriteLine("Inside passByVal() method");
            num1 = num1+5;
            Console.WriteLine("added 5 to number passed by value = "+num1);
        }
        public static void passByRef (ref double num1){
            Console.WriteLine("Inside passByVal() method");
            num1 = num1+5;
            Console.WriteLine("added 5 to number passed by value = "+num1);
        }
    }
}
