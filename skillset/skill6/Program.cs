﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime DT = DateTime.Now;
            string requirements =
            @"
            
            program does not perform data validation
            author: zach rodefer
            date: "+ DT.ToString();
            
            Console.Write(requirements);
            Console.Write("\nNumber 1:");
            double num1 = double.Parse(Console.ReadLine());
            Console.Write("\nNumber 2:");
            double num2 = double.Parse(Console.ReadLine());
            string operations =
            @"
            1 - addition
            2 - subtraction
            3 - multiplication
            4 - division
            5 - exponentiation";
            Console.Write(operations);
            Console.Write("\nChoose a mathematical operation.");
            int mathOp = int.Parse(Console.ReadLine());
            switch (mathOp){
                case 1:
                    double numAd=add(num1, num2);
                    Console.WriteLine("Result of addition operation: "+numAd);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();

                    break;
                case 2:
                    double numSub=sub(num1, num2);
                    Console.WriteLine("Result of subtraction operation: "+numSub);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
                case 3:
                    double numMulti=multi(num1, num2);
                    Console.WriteLine("Result of multiplication operation: "+numMulti);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
                case 4:
                    if (num2 == 0){
                        Console.WriteLine("Sorry, Division by zero is not permitted.");
                        Console.WriteLine("press any key to exit");
                        Console.ReadKey();
                        Environment.Exit(1);
                    }
                    double numDiv=div(num1, num2);
                    Console.WriteLine("Result of division operation: "+numDiv);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
                case 5:
                    double numExpo=expo(num1, num2);
                    Console.WriteLine("Result of exponential operation: "+numExpo);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
                default:
                    Console.WriteLine("An incorrect mathematical operation was entered.");
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
        }
    }
        public static double add(double num1, double num2){
                    double numAdd = num1 + num2;
                    return numAdd;
    }
        public static double sub(double num1, double num2){
                    double numAdd = num1 - num2;
                    return numAdd;
    }
            public static double multi(double num1, double num2){
                    double numAdd = num1*num2;
                    return numAdd;
    }
            public static double div(double num1, double num2){
                    double numAdd = num1 /num2;
                    return numAdd;
    }
            public static double expo(double num1, double num2){
                    double numAdd = Math.Pow(num1 , num2);
                    return numAdd;
    }
    
}
}
