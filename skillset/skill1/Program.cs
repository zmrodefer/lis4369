﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Please enter your full name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Hello, "+ name);
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            Environment.Exit(1);
        }
    }
}
