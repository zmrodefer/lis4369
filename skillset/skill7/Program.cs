﻿using System;

namespace ConsoleApplication
{
    public class Program
    {

        public static void Main(string[] args)
        {
            DateTime DT = DateTime.Now;
            string requirements =
            @"
Program uses Default constructors with getters
and setters and paramaterized constructors to
Create and modify new person objects
author: zach rodefer
date: "+ DT.ToString()+"\n\n";        
            Console.Write(requirements);

            
            Console.WriteLine("Creating Person object from default constructor (accepts no arguments)");
            Person p = new Person();
            string fn = p.getFName();
            string ln = p.getLName();
            DisplayNames(fn,ln);
            
            Console.WriteLine("\nModify default constructor's object values");       
            p.setFName("Zach");
            p.setLName("Rodefer");
            fn = p.getFName();
            ln = p.getLName();
            DisplayNames(fn,ln);
            
            Console.WriteLine("\ncalling parameterized constructor(accepts two arguemnts)");
            Console.Write("First Name: ");
            string PCFname = Console.ReadLine();
            Console.Write("Last Name: ");
            string PCLname = Console.ReadLine();

            
            Console.WriteLine("\ncreating paramaterized constructor(Accepts two areguments)");
            Person p2 = new Person(PCFname, PCLname);
            fn = p2.getFName();
            ln = p2.getLName();
            Console.WriteLine("Full Name: "+fn+" "+ln);
            
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            
            
        }
        public static void DisplayNames(string fn, string ln){
            
            Console.WriteLine("First Name: "+fn);
            Console.WriteLine("Last Name: "+ln);
        }


    }
}
