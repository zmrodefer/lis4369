using System;

namespace ConsoleApplication
{
    public class Person : Program
    {
        private string FName,LName;
        public Person(){
            FName = "billy";
            LName = "Joel";
        }
        public Person(string FName, string LName){
            this.FName = FName;
            this.LName = LName;
        }
        public string getFName(){
            return FName;
        }
        public string getLName(){
            return LName;
        }
        public void setFName(string FName){
            this.FName=FName;
        }
        public void setLName(string LName){
            this.LName=LName;
        }
    }
}