﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime DT = DateTime.Now;
            string requirements =
            @"
            Assignment 2: a simple calculator
            program does not perform data validation
            author: zach rodefer
            date: "+ DT.ToString();
            
            Console.Write(requirements);
            Console.Write("\nNumber 1:");
            int num1 = int.Parse(Console.ReadLine());
            Console.Write("\nNumber 2:");
            int num2 = int.Parse(Console.ReadLine());
            string operations =
            @"
            1 - addition
            2 - subtraction
            3 - multiplication
            4 - division
            5 - exponentiation";
            Console.Write(operations);
            Console.Write("\nChoose a mathematical operation.");
            int mathOp = int.Parse(Console.ReadLine());
            switch (mathOp){
                case 1:
                    add(num1, num2);

                    break;
                case 2:
                    sub(num1, num2);
                    break;
                case 3:
                    multi(num1, num2);
                    break;
                case 4:
                    if (num2 == 0){
                        Console.WriteLine("Sorry, Division by zero is not permitted.");
                        Console.WriteLine("press any key to exit");
                        Console.ReadKey();
                        Environment.Exit(1);
                    }
                    div(num1, num2);
                    break;
                case 5:
                    expo(num1, num2);
                    break;
                default:
                    Console.WriteLine("An incorrect mathematical operation was entered.");
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
        }
    }
        public static void add(int num1, int num2){
                    double numAdd = Convert.ToDouble(num1) + Convert.ToDouble(num2);
                    Console.WriteLine("Result of addition operation: "+numAdd);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
    }
        public static void sub(int num1, int num2){
                    double numAdd = Convert.ToDouble(num1) - Convert.ToDouble(num2);
                    Console.WriteLine("Result of subtraction operation: "+numAdd);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
    }
            public static void multi(int num1, int num2){
                    double numAdd = Convert.ToDouble(num1) * Convert.ToDouble(num2);
                    Console.WriteLine("Result of multiplication operation: "+numAdd);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
    }
            public static void div(int num1, int num2){
                    double numAdd = Convert.ToDouble(num1) / Convert.ToDouble(num2);
                    Console.WriteLine("Result of division operation: "+numAdd);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
    }
            public static void expo(int num1, int num2){
                    double numAdd = Math.Pow(Convert.ToDouble(num1) , Convert.ToDouble(num2));
                    Console.WriteLine("Result of exponential operation: "+numAdd);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
    }
    
}
}
