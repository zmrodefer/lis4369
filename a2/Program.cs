﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime DT = DateTime.Now;
            string requirements =
            @"
            Assignment 2: a simple calculator
            program does not perform data validation
            author: zach rodefer
            date: "+ DT.ToString();
            
            Console.Write(requirements);
            Console.Write("\nNumber 1:");
            int num1 = int.Parse(Console.ReadLine());
            Console.Write("\nNumber 2:");
            int num2 = int.Parse(Console.ReadLine());
            string operations =
            @"
            1 - addition
            2 - subtraction
            3 - multiplication
            4 - division";
            Console.Write(operations);
            Console.Write("\nChoose a mathematical operation.");
            int mathOp = int.Parse(Console.ReadLine());
            switch (mathOp){
                case 1:
                    double numAdd = Convert.ToDouble(num1) + Convert.ToDouble(num2);
                    Console.WriteLine("Result of addition operation: "+numAdd);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
                case 2:
                    double numSub = Convert.ToDouble(num1) - Convert.ToDouble(num2);
                    Console.WriteLine("Result of subtraction operation: "+numSub);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
                case 3:
                    double numMulti = Convert.ToDouble(num1) * Convert.ToDouble(num2);
                    Console.WriteLine("Result of multiplication operation: "+numMulti);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
                case 4:
                    if (num2 == 0){
                        Console.WriteLine("Sorry, Division by zero is not permitted.");
                        Console.WriteLine("press any key to exit");
                        Console.ReadKey();
                        Environment.Exit(1);
                    }
                    double numDiv = Convert.ToDouble(num1) / Convert.ToDouble(num2);
                    Console.WriteLine("Result of division operation: "+numDiv);
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
                default:
                    Console.WriteLine("An incorrect mathematical operation was entered.");
                    Console.WriteLine("press any key to exit");
                    Console.ReadKey();
                    break;
            }
        }
    }
}
