> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Zach Rodefer


*Assignment 1 Read me*
[Assignment 1 README.md](https://bitbucket.org/Zmr13FSUIT/lis4369/src/87c87b4f1e6bf8b3442933562a51f8c38c4f73a0/a1/?at=master "assignment 1")

    Download and install .NET core sdk
    Create hwapp and aspcorenetapp

*Assignment 2 Read me*
[Assignment 2 README.md](https://bitbucket.org/Zmr13FSUIT/lis4369/src/d082a017a9f44b7920c08e7e42af2fe640ee6879/a2/?at=master "assignment 2")

    Create a simple calculator with 4 functions
    Crate a DateTime object
    Create an error when a user tries to divide by 0
    
*Assignment 3 Read me*
[Assignment 3 README.md](https://bitbucket.org/Zmr13FSUIT/lis4369/src/f7033e2edfa26f7711bd2b53b0ae41d0385da685/a3/?at=master "assignment 3")

    use intrinsic methods to display date/time
    reasearch future value and its formula
    create FutureValue method using the following parameters: decimal PresentValue, int NumYears, decimal yearlyInt, decimal monthlyDep
    initialize suitable variables; use decimal data type for currency values
    perform data validation; promt user until correct data is entered
    display money in currency format
    allow user to press any key to return to command line
    
*Project 1 Read me*
[Project 1 README.md](https://bitbucket.org/Zmr13FSUIT/lis4369/src/a893acbf4e71029b14358d8b9284d513bfdd79b4/p1/?at=master "Project 1")

    use intrinsic methods to display date/time
    Create Room class
    Create private data fields in Room class
    Create getters and setters
    Allow user to press any key to return to command line.
    
*Assignment 4 Read me*
[Assignment 4 README.md](https://bitbucket.org/Zmr13FSUIT/lis4369/src/5786d7e37176a1365ed2b366db86886b8264930d/a4/?at=master "Assignment 4")

    use intrinsic methods to display date/time
    Create person class with 3 Protected data members(fname lname and age)
    Create getters/setters and default/paramaterized constructors
    Create Student subclass of person with 3 private data members (college major and gpa)
    Create gettersand default/paramaterized constructors that extend the base constructors
    implement classes in main method
    allow user to press any key to return to command line
    
*Project 1 Read me*
[Project 1 README.md](https://bitbucket.org/Zmr13FSUIT/lis4369/src/d583ab662fba37eb0e55b805777e7bdec9cc7cd6/p2/?at=master "Project 1")

    prompt user for Last name. Return full name occupation age
    prompt user for age and occupation. Return Full name
    Allow user to return back to command line by pressing any key