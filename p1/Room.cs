using System;

namespace ConsoleApplication
{
    public class Room : Program
    {
        private string type;
        private double length,width, height;
        public Room(){
            Console.WriteLine("Creating default room object from default constructor (accepts no arguments)");
            type = "Default";
            length = 10;
            height = 10;
            width = 10;
        }
        public Room(string type, double length, double width, double height){
            this.type = type;
            this.length = length;
            this.width = width;
            this.height = height;
        }
        public void setType( string type ){
            this.type = type;
        }
        public void setLength( double length ){
            this.length = length;
        }
        public void setHeight( double height){
            this.height = height;
        }
        public void setWidth( double width ){
            this.width = width;
        }
        public string getType(){
            return type;
        }
        public double getHeight(){
            return height;
        }
        public double getLength(){
            return length;
        }
        public double getWidth(){
            return width;
        }
        public double getArea(){
            return length*width;
        }
        public double getVolume(){
            return length*width*height;
        }
        
    }
}
