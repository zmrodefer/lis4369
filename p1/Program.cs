﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime DT = DateTime.Now;
            string requirements =
            @"
Program uses Default constructors with getters
and setters and paramaterized constructors to
Create and modify new Room objects
author: zach rodefer
date: "+ DT.ToString()+"\n\n";        
            Console.Write(requirements);
            

            Room r1 = new Room();
            string type1 = r1.getType();
            double length1 = r1.getLength();
            double height1 = r1.getHeight();
            double width1 = r1.getWidth();
            double area1 = r1.getArea();
            double volume1 = r1.getVolume();
            double length2,width2,height2;
            string type2;
            DisplayProperties(type1, length1,width1,height1,area1,volume1);
            
            Console.WriteLine("\nModify default object's propeties: \n");
            Console.Write("Room type: ");
            type1 = Console.ReadLine();
            Console.Write("Room length: ");
            while(!double.TryParse(Console.ReadLine(), out length1)){
                Console.WriteLine("Room Length must be a double data type");
                Console.Write("Room length: ");
            }
            Console.Write("Room Width: ");
            while(!double.TryParse(Console.ReadLine(), out width1)){
                Console.WriteLine("Room Width must be a double data type");
                Console.Write("Room Width: ");
            }
            Console.Write("Room Height: ");
            while(!double.TryParse(Console.ReadLine(), out height1)){
                Console.WriteLine("Room Height must be a double data type");
                Console.Write("Room Height: ");
            }
            r1.setType(type1);
            r1.setLength(length1);
            r1.setWidth(width1);
            r1.setHeight(height1);
            
            Console.WriteLine("\nDisplay default Room object's updated values: ");
            DisplayProperties(type1, length1,width1,height1,r1.getArea(),r1.getVolume());
            
            Console.WriteLine("\nCall Paramaterized construsctor(accepts 4 arguments)\n");
            Console.Write("Room type: ");
            type2 = Console.ReadLine();
            Console.Write("Room length: ");
            while(!double.TryParse(Console.ReadLine(), out length2)){
                Console.WriteLine("Room Length must be a double data type");
                Console.Write("Room length: ");
            }
            Console.Write("Room Width: ");
            while(!double.TryParse(Console.ReadLine(), out width2)){
                Console.WriteLine("Room Width must be a double data type");
                Console.Write("Room Width: ");
            }
            Console.Write("Room Height: ");
            while(!double.TryParse(Console.ReadLine(), out height2)){
                Console.WriteLine("Room Height must be a double data type");
                Console.Write("Room Height: ");
            }
            Room r2 = new Room(type2,length2,width2,height2);
            
            Console.WriteLine("\nCreating a new room object from parameterized constructor");
            DisplayProperties(type2, length2,width2,height2,r2.getArea(),r2.getVolume());
            
            Console.WriteLine("\nPress any key to exit");
            Console.ReadKey();
            
        }
        public static void DisplayProperties(string type,double length, double width, double height, double area, double volume){
            string Properties =
            @"
Room Type: "+ type+
            "\nRoom Length: "+ length.ToString("F2")+
            "\nRoom Width: "+ width.ToString("F2")+
            "\nRoom Height: "+ height.ToString("F2")+
            "\nRoom Area: "+ area.ToString("F2")+ " sq ft"+
            "\nroom volume: "+ volume.ToString("F2")+ " cu ft"+
            "\nroom volume: "+ (volume/27).ToString("F2")+ " cu yd\n";
            Console.Write(Properties);
        }
    }
}
