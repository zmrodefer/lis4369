﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime DT = DateTime.Now;
            string requirements =
            @"
            Assignment 4: Extending classes
            1: use intrinsic methods to display date/time
            2: Create person class with 3 Protected data members(fname lname and age)
            3: Create getters/setters and default/paramaterized constructors
            4: Create Student subclass of person with 3 private data members (college major and gpa)
            5: Create gettersand default/paramaterized constructors that extend the base constructors
            6: implement classes in main method
            7: allow user to press any key to return to command line
            author: zach rodefer
            date: "+ DT.ToString();
            Console.Write(requirements);
            Console.WriteLine();
            Console.WriteLine();
            
            // new person object----------
            Person p1 = new Person();
            perProperties(p1);
            
            
            
            //Entering new data to modify p1-----------
            Console.WriteLine("\n\n-------------------------------\n\n");
            Console.WriteLine("Modify the default constructors base values using getter/setter methods");
            Console.Write("First name:");
            p1.setFname(Console.ReadLine());
            
            Console.Write("Last name: ");
            p1.setLname(Console.ReadLine());
            
            Console.Write("Age: ");
            int ageTemp;
            while(!int.TryParse(Console.ReadLine(), out ageTemp)){
                Console.WriteLine("age must be an int data type");
                Console.Write("age: ");
            }
            p1.setAge(ageTemp);
            
            Console.WriteLine("\n\n-------------------------------\n\n");
            Console.WriteLine("Objects new data member values");
            perProperties(p1);
            
            
            
            // Entering data for p2--------
            Console.WriteLine("\n\n-------------------------------\n\n");
            Console.WriteLine("Creating new person object from parameterized constructor (please enter arguments)");
            Console.Write("First name:");
            string fnameTemp = Console.ReadLine();
            
            Console.Write("Last name:");
            string lnameTemp = Console.ReadLine();

            Console.Write("Age: ");
            while(!int.TryParse(Console.ReadLine(), out ageTemp)){
                Console.WriteLine("age must be an int data type");
                Console.Write("Age: ");
            }
            
            
            //Creating p2 ----------
            Console.WriteLine("\n\n-------------------------------\n\n");
            Person p2 = new Person(fnameTemp, lnameTemp, ageTemp);
            perProperties(p2);
            
            //Creating student object with default constructor
            Console.WriteLine("\n\n-------------------------------\n\n");
            Student s1 = new Student();
            perProperties(s1);
            
            //Creating student object with parameterized constructor
            Console.WriteLine("\n\n-------------------------------\n\n");
            Console.WriteLine("Demonstrating Polymorphism (new derived object)\n (Calling parameterized base class constructor explicitly)");
            Console.WriteLine("Creating new person object from parameterized constructor (please enter arguments)");
            Console.Write("First name:");
            fnameTemp = Console.ReadLine();
            
            Console.Write("Last name:");
            lnameTemp = Console.ReadLine();

            Console.Write("Age: ");
            while(!int.TryParse(Console.ReadLine(), out ageTemp)){
                Console.WriteLine("age must be an int data type");
                Console.Write("Age: ");
            }
            Console.Write("College: ");
            string collegeTemp = Console.ReadLine();
            
            Console.Write("major: ");
            string majorTemp = Console.ReadLine();
            
            double gpaTemp;
            Console.Write("GPA: ");
            while(!double.TryParse(Console.ReadLine(), out gpaTemp)){
                Console.WriteLine("GPA must be a double data type");
                Console.Write("GPA: ");
            }
            //creating student object from entered data
            Console.WriteLine("\n\n-------------------------------\n\n");
            Student s2 = new Student(fnameTemp, lnameTemp, ageTemp, collegeTemp, majorTemp, gpaTemp);
            perProperties(s2);
            
            Console.WriteLine("\n\n-------------------------------\n\n");
            Console.WriteLine("Person2 -- getObjectInfo(virtual)");
            Console.WriteLine(p2.getObjectInfo());
            
            Console.WriteLine("\n\n-------------------------------\n\n");
            Console.WriteLine("Student2 -- getObjectInfor(overridden)");
            Console.WriteLine(s2.getObjectInfo());
            
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            
            
        }
        public static void perProperties ( Person p){ // this method prints data members of person class and the student class if applicable
            Console.WriteLine("First name: "+ p.getFname());
            Console.WriteLine("Last Name: "+ p.getLname());
            Console.WriteLine("Age: "+ p.getAge());
            if( p is Student){
            Console.WriteLine("College: "+ ((Student)p).getCollege());
            Console.WriteLine("Major: "+ ((Student)p).getMajor());
            Console.WriteLine("GPA: "+ ((Student)p).getGpa());
            }
        }
    }
}
