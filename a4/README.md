
# LIS 4381

## Zach Rodefer

###Requirements:

    Assignment 4: Extending classes
    1: use intrinsic methods to display date/time
    2: Create person class with 3 Protected data members(fname lname and age)
    3: Create getters/setters and default/paramaterized constructors
    4: Create Student subclass of person with 3 private data members (college major and gpa)
    5: Create gettersand default/paramaterized constructors that extend the base constructors
    6: implement classes in main method
    7: allow user to press any key to return to command line


#### README.md file should include the following items:


#### Assignment Screenshots:

*Screenshot of Program running:

![Program running](img/a1.png)
![Program running](img/a2.png)
![Program running](img/a3.png)





