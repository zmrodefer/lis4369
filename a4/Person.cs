using System;

namespace ConsoleApplication
{
    public class Person
    {
        protected string lname, fname;
        protected int age;
        
        public Person(){
            Console.WriteLine("Creating first and last name person object from default constructor (accepts no arguments)");
            fname = "First Name";
            lname = "Last Name";
            age = 0;
        }
        
        public Person(string fname, string lname, int age){
            Console.WriteLine("Creating base Person constructor (accepts arguments)");
            this.fname = fname;
            this.lname = lname;
            this.age = age;
        }
        
        public void setLname (string lname){
            this.lname = lname;
        }
        public void setFname (string fname){
            this.fname = fname;
        }
        public void setAge (int age){
            this.age = age;
        }
        public string getLname (){
            return lname;
        }
        public string getFname (){
            return fname;
        }
        public int getAge (){
            return age;
        }
        public virtual string getObjectInfo (){
            return fname + " " + lname + " is " + age;
        }

    }
}