using System;

namespace ConsoleApplication
{
    public class Student : Person
    {
        private string college, major;
        private double gpa;
        public Student(){
            Console.WriteLine("Creating default student object from default constructor(accepts no arguments)");
            college = "CCI";
            major = "IT";
            gpa = 3.99;
            
        }
        public Student(string fname, string lname, int age, string college, string major, double gpa) : base(fname, lname, age){
            Console.WriteLine("creating default student object from parameterized constructor(accepts arguments)");
            this.college = college;
            this.major = major;
            this.gpa = gpa;
        }
        
        public string getName(){
            return getFname() + " " + getLname();
        }
        public string getFullName(){
            return fname + " " + lname;
        }
        public override string getObjectInfo(){
            return base.getObjectInfo() + " in the college of "+ this.college + ", majoring in "+ this.major+ ", with a GPA of " + this.gpa + ".";
        }
        public string getCollege(){
            return college;
        }
        public string getMajor(){
            return major;
        }
        public double getGpa(){
            return gpa;
        }

    }
}