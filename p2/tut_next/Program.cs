﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main()
        {
            DateTime DT = DateTime.Now;
            string requirements =
            @"
            Project 1: Using LINQ
            1: prompt user for Last name. Return full name occupation age
            2: prompt user for age and occupation. Return Full name
            3: Allow user to return back to command line by pressing any key
            author: zach rodefer
            date: "+ DT.ToString();
            Console.Write(requirements);
            Console.WriteLine();
            Console.WriteLine();
            var people = GenerateListOfPeople();

        // Write your code here
            
            Console.Write("Last Name(Case sensitive): ");
            String lName = Console.ReadLine();
            var peopleWithlname = people.Where(x=> x.LastName == lName);
            Console.WriteLine("people matching that name:");
            foreach( var Person in peopleWithlname){
                
                Console.WriteLine($"{Person.FirstName} {Person.LastName} is a {Person.Occupation} and is {Person.Age} years old.");
            }
            Console.WriteLine();
            int ageIn;
            Console.Write("Age: ");
            while(!int.TryParse(Console.ReadLine(), out ageIn))
            {
                Console.WriteLine("age must be an interger value");
            }
            Console.Write("Occupation(Dev or Manager):");
            string occIn = Console.ReadLine();
            
            var peopleOverAgeIn = people.Where(x=> x.Age == ageIn && x.Occupation == occIn );
            foreach( var Person in peopleOverAgeIn){
                
                Console.WriteLine($"Matching Criteria: {Person.FirstName}, {Person.LastName}");
            }
            
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            
        }

        public static List<Person> GenerateListOfPeople()
        {
            var people = new List<Person>();

            people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
            people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
            people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
            people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
            people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });

            return people;
        }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Occupation { get; set; }
        public int Age { get; set; }
    }
}
