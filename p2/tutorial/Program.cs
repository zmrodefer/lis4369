﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication
{
    public class Program
{
    public static void Main()
    {
        var people = GenerateListOfPeople();

        // Write your code here
        Console.WriteLine("Finding Items in Collections");
        
        //There will be two Persons in this variable: the "Steve" Person and the "Jane" Person
        Console.WriteLine("Where:");
        var peopleOverTheAgeOf30 = people.Where(x => x.Age > 30);
        foreach( var Person in peopleOverTheAgeOf30)
        {
            Console.WriteLine($"{Person.FirstName}, {Person.LastName}");
        }
        Console.WriteLine();
        //                     -----
        //                     -----
        //Will ignore Eric and Steve in the list of people
        Console.WriteLine("Skip:");
        IEnumerable<Person> afterTwo = people.Skip(2);
        foreach( var Person in afterTwo)
        {
            Console.WriteLine($"{Person.FirstName}, {Person.LastName}");
        }
        Console.WriteLine();
        //                     -----
        //                     -----
        //Will only return Eric and Steve from the list of people
        Console.WriteLine("Take:");
        IEnumerable<Person> takeTwo = people.Take(2);
        foreach( var Person in takeTwo)
        {
            Console.WriteLine($"{Person.FirstName}, {Person.LastName}");
        }
        Console.WriteLine();
        //                     -----
        //                     -----
        //                     -----
        Console.WriteLine("Changing each item in Collections");
        
        Console.WriteLine("Select:");
        IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
        IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
        
        foreach( string name in allFirstNames)
        {
            Console.WriteLine(name);
        }
        Console.WriteLine();
        foreach( var fullName in allFullNames)
        {
            Console.WriteLine($"{fullName.Last}, {fullName.First}");
        }
        Console.WriteLine();
        
        Console.WriteLine("Finding One Item In Collections");
        Console.WriteLine("FirstOrDefault:");
        Person firstOrDefault = people.FirstOrDefault();
        Console.WriteLine(firstOrDefault.FirstName); //Will output "Eric"
        //                     -----
        //                     -----
        Console.WriteLine();
        Console.WriteLine("Conditional FirstOrDefault:");
        var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
        var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
        Console.WriteLine(firstThirtyYearOld1.FirstName); //Will output "Brendan"
        Console.WriteLine(firstThirtyYearOld2.FirstName); //Will also output "Brendan"
        
        Console.WriteLine();
        
        List<Person> emptyList = new List<Person>(); // Empty collection
        Person willBeNull = emptyList.FirstOrDefault(); // None - default of null used

        List<Person> people2 = GenerateListOfPeople();
        Person willAlsoBeNull = people2.FirstOrDefault(x => x.FirstName == "John"); // No John - default of null used
        Console.WriteLine("FirstOrDefault on empty lists return null:");
        Console.WriteLine("is null?"+ willBeNull == null); // true
        Console.WriteLine("is null?"+ willAlsoBeNull == null); //true
        
        Console.WriteLine();
        Console.WriteLine("SingleOrDefault:\n(Will return single object if only one exists, otherwise throws InvalidOperationException)\n");
        Person single = people.SingleOrDefault(x => x.FirstName == "Eric"); //Will return the Eric Person obejct
        Console.WriteLine(single.FirstName);
        try
        {
        Person singleDev = people.SingleOrDefault(x => x.Occupation == "Dev"); //Will throw the System.InvalidOperationException
        }
        catch(InvalidOperationException e){
            Console.WriteLine();
            Console.WriteLine(e);
            Console.WriteLine();
        }
        
        Console.WriteLine("LastorDefault:");
        Person lastOrDefault = people.LastOrDefault();
        Console.WriteLine(lastOrDefault.FirstName); //Will output "Samantha"
        Console.WriteLine();
        Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
        Console.WriteLine(lastThirtyYearOld.FirstName); //Will output "Brendan"
        Console.WriteLine();
        
        Console.WriteLine("Finding Data About collections\n");
        
        Console.WriteLine("Count:\n Number of people:");
        int numberOfPeopleInList = people.Count(); //Will return 5
        Console.WriteLine(numberOfPeopleInList);
        
        Console.WriteLine();
        
        Console.WriteLine("Any(returns bool value):");
        bool thereArePeople = people.Any(); //This will return true
        bool thereAreNoPeople = emptyList.Any(); //This will return false
        Console.WriteLine("Any objects in list people: "+ thereArePeople + " , "+"\nAny objects in list empyList"+ thereAreNoPeople);
        
        Console.WriteLine("All (returns bool value)");
        bool allDevs = people.All(x => x.Occupation == "Dev"); //Will return false
        bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24); //Will return true
        
        Console.WriteLine("all objects in list people have occupation \"dev\":"+allDevs + " , \n all objects in list people over age 24" + everyoneAtLeastTwentyFour+"\n");
        
        Console.WriteLine("\nConverting Results To Collections");
        
        Console.WriteLine("Tolist converts results to list:");
        List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList(); //This will return a List<Person>
        
        foreach(var peoples in listOfDevs)
        {
            Console.WriteLine($"{peoples.FirstName}, {peoples.LastName}");
        }
        
        Console.WriteLine("ToArray converts results to array: ");
        Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray(); //This will return a Person[] array
        
        foreach(var peoples in arrayOfDevs)
        {
            Console.WriteLine($"\n{peoples.FirstName}, {peoples.LastName}");
        }
        
    }

    public static List<Person> GenerateListOfPeople()
    {
        var people = new List<Person>();

        people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
        people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
        people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
        people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
        people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });

        return people;
    }
}

public class Person
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Occupation { get; set; }
    public int Age { get; set; }
}
public class FullName
{
    public string First { get; set; }
    public string Last { get; set; }
}
}