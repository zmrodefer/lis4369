﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
             DateTime DT = DateTime.Now;
            string requirements =
            @"
            Assignment 3: future value calculator
            1: use intrinsic methods to display date/time
            2: reasearch future value and its formula
            3: create FutureValue method using the following parameters: decimal PresentValue, int NumYears, decimal yearlyInt, decimal monthlyDep
            4: initialize suitable variables; use decimal data type for currency values
            5: perform data validation; promt user until correct data is entered
            6: display money in currency format
            7:allow user to press any key to return to command line
            author: zach rodefer
            date: "+ DT.ToString();
            
            Console.Write(requirements);
            decimal presentValue, yearlyInt, monthlyDep;
            int numYears;
            Console.Write("\n\nenter starting value:");
            bool resSV = decimal.TryParse(Console.ReadLine(), out presentValue);
            while(resSV == false) {
                Console.Write("\nValue must be a decimal data type.\n");
                Console.Write("enter starting value:");
                resSV = decimal.TryParse(Console.ReadLine(), out presentValue);
            }
            Console.Write("\n\nenter term (years):");
            bool resNY = int.TryParse(Console.ReadLine(), out numYears);
            while(resNY == false) {
                Console.Write("\nValue must be a int data type.\n");
                Console.Write("enter term (years):");
                resNY = int.TryParse(Console.ReadLine(), out numYears);
            }
            Console.Write("\n\nenter Yearly Interest rate:");
            bool resYI = decimal.TryParse(Console.ReadLine(), out yearlyInt);
            while(resYI == false) {
                Console.Write("\nValue must be a decimal data type.\n");
                Console.Write("enter Yearly Interest rate:");
                resYI = decimal.TryParse(Console.ReadLine(), out yearlyInt);
            }
            Console.Write("\n\nenter the monthly deposit:");
            bool resMD = decimal.TryParse(Console.ReadLine(), out monthlyDep);
            while(resMD == false) {
                Console.Write("\nValue must be a decimal data type.\n");
                Console.Write("enter the monthly deposit:");
                resMD = decimal.TryParse(Console.ReadLine(), out monthlyDep);
            }
            decimal value = futureValue(presentValue, numYears, yearlyInt, monthlyDep);
            Console.WriteLine("Future Value: "+value.ToString("C2"));
            Console.WriteLine("press any key to exit");
            Console.ReadKey();
        
            
        }
        public static decimal futureValue(decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep){
            decimal pvInt = (decimal)Math.Pow(1+((double)yearlyInt/1200), numYears*12);
            decimal PVtoFV = presentValue * pvInt;
            decimal MDtoFV = (monthlyDep)*(pvInt-1)/(yearlyInt/1200);
            return MDtoFV+PVtoFV;
        }
    }
}
