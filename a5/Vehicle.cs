using System;
    public class Vehicle
    {
        private float milesTraveled, gallonsUsed;
        public string Manufacturer {get; set;}
        public string Make {get; set;}
        public string Model {get; set;}
        
        //---------------------------------
        
        public float MPG
        {
            get {
            if (gallonsUsed <= 0.0f) { return 0.0f; }
            else { return milesTraveled/gallonsUsed; }
                }
        }
        
        //---------------------------------
        
        public Vehicle ()
        {
            Console.WriteLine("Creating base class default object (accepts no arguments)");
            this.Manufacturer = "GM";
            this.Make = "Chevorlet";
            this.Model = "Corvette";
        }
        
        //---------------------------------
        
        public Vehicle ( string Manufacturer = "Manufacturer", string Make = "Make", string Model = "Model")
        {
            Console.WriteLine("Creating base class parameterized object (accepts arguments)");
            this.Manufacturer = Manufacturer;
            this.Make = Make;
            this.Model = Model;
        }
        
        //---------------------------------
        
        public void setMiles ( float milesTraveled = 0.0f)
        {
            this.milesTraveled = milesTraveled;
        }
        
        //---------------------------------
        
        public void setGallons ( float gallonsUsed = 0.0f)
        {
            this.gallonsUsed = gallonsUsed;
        }
        
        //---------------------------------
        
        public float getMiles()
        {
            return milesTraveled;
        }
        
        //---------------------------------
        
        public float getGallons()
        {
            return gallonsUsed;
        }
        
        //---------------------------------
        
        public virtual string getObjectInfo()
        {
            return Manufacturer + " - "+ Make + " - " + Model + " - "+ MPG;
        }
        
        //---------------------------------
        
        public virtual string getObjectInfo( string sep)
        {
            return Manufacturer + sep + Make + sep + Model + sep + MPG;
        }
        
    }