    using System;
    public class Car : Vehicle
    {
        private string style;
        
        //---------------------------------
        
        public string Style
        { get {return style;}
          set {style = value;}
        }
        
        //---------------------------------
        
        public Car ()
        {
            Console.WriteLine("Creating derived object from default constructor(accepts no arguments)");
            this.Style = "Default Style";
        }
        
        //---------------------------------
        
        public Car ( string Manufacturer , string Make, string Model, string Style) : base(Manufacturer, Make, Model)
        {
            Console.WriteLine("Creating derived object from parameterized constructor (accepts arguments)");
            this.Manufacturer = Manufacturer;
            this.Make = Make;
            this.Model = Model;
            this.Style = Style;
        }
        
        //---------------------------------
        
        public override string getObjectInfo(string sep)
        {
            return base.getObjectInfo(sep) + sep + Style;
        }
        

    }