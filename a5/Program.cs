﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime DT = DateTime.Now;
            string requirements =
            @"
            Assignment 5: Inheritance in classes
            1: Create Vehicle base class
            2: Create Car child class
            3: instantiate 3 vehicle objects
            4: instantiate 1 car object
            5: include data validation
            6: let user press any key to exit.
            author: zach rodefer
            date: "+ DT.ToString();
            Console.Write(requirements);
            Console.WriteLine("\n\n-------------------------------\n\n");
            
            string delimiter;
            string p_Manufacturer, p_Make, p_Model, p_Style;
            float p_Miles, p_Gallons;
            
            Console.WriteLine("Vehicle1 - instantiating new object from default constructor");
            Vehicle v1 = new Vehicle();
            Console.WriteLine(v1.getObjectInfo());
            
            Console.WriteLine("Vehicle 2 - instantiating new object from parameterized base constructor(accepts arguments");
            Console.Write("Manufacturer (alpha): ");
            p_Manufacturer = Console.ReadLine();
            Console.Write("Make (alpha): ");
            p_Make = Console.ReadLine();
            Console.Write("Model (alpha): ");
            p_Model = Console.ReadLine();
            Console.Write("Miles driven (float): ");
            while(!float.TryParse(Console.ReadLine(), out p_Miles))
            {
                Console.Write("Miles must be numeric: ");
            }
            Console.Write("Gallons Used (float): ");
            while(!float.TryParse(Console.ReadLine(), out p_Gallons))
            {
                Console.Write("Gallons Used must be numeric: ");
            }
            
            Console.WriteLine("\n Vehicle2 - instantiating new object (only passing first argument)");
            Vehicle v2 = new Vehicle(p_Manufacturer);
            Console.WriteLine(v2.getObjectInfo());
            
            Console.WriteLine("Vehicle2 - passing arg to overloaded getObjectInfo()");
            delimiter = Delimiter();
            Console.WriteLine(v2.getObjectInfo(delimiter));
            
            Console.WriteLine("\n Vehicle3 - Instantiating new object(passing all arguments");
            Vehicle v3 = new Vehicle(p_Manufacturer, p_Make, p_Model);
            v3.setMiles(p_Miles);
            v3.setGallons(p_Gallons);
            delimiter = Delimiter();
            Console.WriteLine(v3.getObjectInfo(delimiter));
            
            Console.WriteLine("Demonstrating Polymorphism(new derived object)");
            Console.WriteLine("\n Car1 - calling parameterized base class constructor explicitly");
            

            Console.Write("Manufacturer (alpha): ");
            p_Manufacturer = Console.ReadLine();
            Console.Write("Make (alpha): ");
            p_Make = Console.ReadLine();
            Console.Write("Model (alpha): ");
            p_Model = Console.ReadLine();
            Console.Write("style (alpha): ");
            p_Style = Console.ReadLine();
            Console.Write("Miles driven (float): ");
            while(!float.TryParse(Console.ReadLine(), out p_Miles))
            {
                Console.Write("Miles must be numeric: ");
            }
            Console.Write("Gallons Used (float): ");
            while(!float.TryParse(Console.ReadLine(), out p_Gallons))
            {
                Console.Write("Gallons Used must be numeric: ");
            }
            
            
            
            
        }
        
        public static string Delimiter(){
            Console.Write("Delimiter(; : . ): ");
            return Console.ReadLine();
        }
    }
}
